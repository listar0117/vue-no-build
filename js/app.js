Vue.component('graph',{ 
	name: 'Graph',
	template: `
		<div></div>
	`,
	props: {
		timeList: Array,
		valueList: Array
	},
	data() {
		return {
			chartData: {
				chart: {
					type: 'line'
				},
				title: {
					text: 'Sales Status'
				},
				xAxis: {
					categories: this.timeList
				},
				yAxis: {
					title: {
						text: 'Count'
					}
				},
				series: this.valueList
			}
		}
	},
	mounted() {
		this.drawChart()
	},
	methods: {
		drawChart() {
			$(this.$el).highcharts(this.chartData)
		}
	}
})

const Report = {
	name: 'Report',
	template: `
		<div class="m-2">
			<div class="form-group row">
				<div class="col-12">
					<label class="col-form-label">From</label>
					<input type="date" v-model="startDate">
					<input type="time" v-model="startTime">
					<label class="col-form-label">To</label>
					<input type="date" v-model="endDate">
					<input type="time" v-model="endTime">
					<button class="btn btn-primary" @click="showChart">
						Search
						<span class="spinner-border spinner-border-sm" v-if="isLoading"></span>
					</button>
					<button class="btn btn-info" @click="logout">Logout</button>
				</div>
			</div>
			<graph v-if="!isLoading" :time-list="timeList" :value-list="valueList"></graph>
		</div>
	`,
	data() {
		return {
			startDate: '',
			startTime: '',
			endDate: '',
			endTime: '',
			fromDate: '',
			toDate: '',
			url: 'https://api.thegoodtill.com/api/report/sales/summary',
			timeList: [],
			valueList: [],
			isLoading: false
		}
	},
	methods: {
		showChart() {
			if (!this.startDate || !this.startTime || !this.endDate || !this.endTime) {
				alert("Please select date range.")
			} else {
				this.fromDate = moment(this.startDate + " " + this.startTime).format("DD/MM/YYYY hh:mm A")
				this.toDate = moment(this.endDate + " " + this.endTime).format("DD/MM/YYYY hh:mm A")
				let param = {
					daterange: this.fromDate + " - " + this.toDate
				}
				this.isLoading = true
				let vm = this
				$.ajax({
					type: "POST",
					url: this.url,
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('Authorization'),
						'Outlet-Id': localStorage.getItem('Outlet-Id')
					},
					data: param,
					dataType: 'JSON',
					success: function(response) {
						console.log(response)
						vm.timeList = response.data.sales_details.labels
						vm.valueList = [
							{
								name: "Total Amount",
								data: response.data.sales_details.amount.map(parseFloat)
							},
							{
								name: "No of Sales",
								data: response.data.sales_details.sales
							},
						]
						vm.isLoading = false
					},
					error: function(err) {
						console.log(err)
						vm.isLoading = false
					}
				});
				
			}
		},
		logout() {
			localStorage.setItem('Authorization', '')
			localStorage.setItem('Outlet-Id', '')
			router.replace('/login')
		}
	}
}

const Login = {
	name: 'LoginForm',
	template: `
		<div class="card mx-auto mt-5 login-form">
			<div class="card-header">
				Login
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="subdomain" class="col-sm-3 col-form-label">Sub Domain</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="subdomain" v-model="subdomain" placeholder="Sub Domain">
					</div>
				</div>
				<div class="form-group row">
					<label for="username" class="col-sm-3 col-form-label">User Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="username" v-model="username" placeholder="User Name">
					</div>
				</div>
				<div class="form-group row">
					<label for="password" class="col-sm-3 col-form-label">Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="password" placeholder="Password" v-model="password">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-9 offset-3">
						<button class="btn btn-primary" @click="login">Login</button>
					</div>
				</div>
			</div>
		</div>`,
	data() {
		return {
			subdomain: 'externaldemo',
			username: 'assessment',
			password: '9BJsL3essWhcWFKc',
			url: 'https://api.thegoodtill.com/api/login'
		}
	},
	methods: {
		login() {
			let param = {
				subdomain: this.subdomain,
				username: this.username,
				password: this.password
			}
			$.ajax({
				type: "POST",
				url: this.url,
				data: param,
				dataType: 'JSON',
				success: function(response) {
					console.log(response)
					localStorage.setItem('Authorization', response.token)
					localStorage.setItem('Outlet-Id', response.config.outlet_config.id)
					router.replace('/report')
				},
				error: function(err) {
					console.log(err)
					alert(err.responseJSON.error)
				}
			});
		}
	}
}

const routes = [
  { path: '/login', component: Login },
  { path: '/report', component: Report }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})
if ( !localStorage.getItem('Authorization') || !localStorage.getItem('Outlet-Id')) {
	router.replace('/login')
}	else {
	router.replace('/report')
}

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router
}).$mount('#app')